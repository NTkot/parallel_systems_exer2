#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

void read_dataset_metadata(char* filename, unsigned int *number_of_ponts, unsigned int *number_of_dimensions);
int check_inputs(int N, int p);
void read_dataset_part_of_array(char* filename, double** output);
void swap (double *a, double *b);
void swap_rows(double **arr, unsigned int row1, unsigned int row2);
void print_array(double *arr, unsigned int size, unsigned int local_id);
void print_2d_array(double **arr, unsigned int n, unsigned int m, unsigned int local_id);

#endif