oldpath = addpath(fullfile(matlabroot,'examples','nnet','main'));
filenameImagesTrain = 'train-images-idx3-ubyte.gz';
filenameImagesTest = 't10k-images-idx3-ubyte.gz';

saveFileName = 'MNIST_16384.txt';

XTrain = processImagesMNIST(filenameImagesTrain);
XTest = processImagesMNIST(filenameImagesTest);

XTrain = reshape(XTrain, 28*28, []);
XTrain = extractdata(XTrain);

XTest = reshape(XTest, 28*28, []);
XTest = XTest(:,1:5536);
XTest = extractdata(XTest);

X = [XTrain XTest];
X = X';
X = X(1:16384,:);

outputFile = fopen(saveFileName, 'w');
fprintf(outputFile, '%d %d\n', size(X,1), size(X,2));
dlmwrite(saveFileName, X, 'precision', 10, 'delimiter', ' ', '-append');