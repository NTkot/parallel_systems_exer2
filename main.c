#include <stdio.h>
#include <stdlib.h>
#include <mpi/mpi.h>
#include <sys/time.h>
#include <unistd.h>
#include "utils.h"
#include "mpi_dist.h"

int main(int argc, char *argv[]) {
    FILE *f;
    double **part_of_array;
    double *pivot_point, *distances;
    unsigned int N, d, p, id, pivot;
    struct timeval start, end;
    long elapsed_time;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &p);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);
    if(id == 0) {
        printf("\n");
        if(argc != 2) {
            printf("Usage:\n\n      mpirun -n <number of processes> main <dataset file>\n\nor if you want to start more processes than your logical processors:\n\n      mpirun --oversubscribe -n <number of processes> main <dataset file>\n\n");
            MPI_Abort(MPI_COMM_WORLD, -1);
        }
    }

    MPI_Barrier(MPI_COMM_WORLD);
    read_dataset_metadata(argv[1], &N, &d);
    if(id == 0) {
        // printf("ID%d: N = %u\nd = %u\n", id, N, d);
        int inputs_power_of_two = check_inputs(N,p);
        if(!inputs_power_of_two)
            MPI_Abort(MPI_COMM_WORLD, 2);
    }
    
    pivot_point = (double*)malloc(d * sizeof(double));
    distances = (double*)calloc((N/p), sizeof(double));

    part_of_array = (double**)malloc((N/p) * sizeof(double*));
    part_of_array[0] = (double*)malloc((N/p) * d * sizeof(double));
    for (unsigned int i = 1; i < (N/p); i++)
        part_of_array[i] = part_of_array[i-1] + d;
    read_dataset_part_of_array(argv[1], part_of_array);
    
    if(id == 0) {
        pivot = pivot_select();
        pivot_point = part_of_array[pivot];
    }
    MPI_Bcast(pivot_point, d, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    calculate_pivot_distances(part_of_array, pivot_point, N, d, distances);

    MPI_Barrier(MPI_COMM_WORLD);
    gettimeofday(&start, NULL); // Start counting
    
    distribute_by_median(&distances, part_of_array, N, d, p);

    MPI_Barrier(MPI_COMM_WORLD);
    gettimeofday(&end, NULL);   // End counting

    elapsed_time = (end.tv_sec - start.tv_sec)*1000000 + (end.tv_usec - start.tv_usec); // Elapsed time counted in us
    if(id == 0)
        printf("ID%d: Elapsed time = %ldus\n\n", id, elapsed_time);  // Print time elapsed in microsends


    int result_valid = check_output(part_of_array, pivot_point, N, d);


    free(distances);
    // free(pivot_point);
    // for(int i = 0; i < (N/p); i++)
    //     free(part_of_array[i]);
    // free(part_of_array[0]);
    free(part_of_array);

    MPI_Finalize();
    
    return !result_valid;
}