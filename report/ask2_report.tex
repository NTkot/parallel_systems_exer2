\documentclass{article}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[hidelinks, colorlinks=true, urlcolor=blue, linkcolor=black, citecolor=black]{hyperref}
\usepackage{eso-pic}
\usepackage{array}
\usepackage[utf8]{inputenc}
\usepackage[justification=centering]{caption}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{geometry}
\usepackage{listings}
\geometry{
	a4paper,
	total={150mm,230mm}
}

\makeatletter
\newenvironment{breakablealgorithm}
{% \begin{breakablealgorithm}
		\begin{center}
			\refstepcounter{algorithm}% New algorithm
			\hrule height.8pt depth0pt \kern2pt% \@fs@pre for \@fs@ruled
			\renewcommand{\caption}[2][\relax]{% Make a new \caption
				{\raggedright\textbf{\fname@algorithm~\thealgorithm} ##2\par}%
				\ifx\relax##1\relax % #1 is \relax
				\addcontentsline{loa}{algorithm}{\protect\numberline{\thealgorithm}##2}%
				\else % #1 is not \relax
				\addcontentsline{loa}{algorithm}{\protect\numberline{\thealgorithm}##1}%
				\fi
				\kern2pt\hrule\kern2pt
			}
		}{% \end{breakablealgorithm}
		\kern2pt\hrule\relax% \@fs@post for \@fs@ruled
	\end{center}
}
\makeatother


\begin{document}
\begin{titlepage}
	\begin{center}
		\vspace*{-1cm}
		
		\Huge
		\textbf{Parallel \& Distributed Systems}
		
		\vspace{2cm}
		
		%\LARGE
		\Large
		\textbf{Project 2: Partition of Large Scale Data}
		
		\textbf{based on pivot distance}
		
		\vspace{4cm}
		
		
		\centering
		\includegraphics[width=12cm,keepaspectratio]{images/title.jpg}
		
		\vfill
		
		\vspace{1.25cm}
		\Large
		Nikolaos Kotarelas 9106
		\vspace{0.25cm}
		\\December 2021
		\vspace{0.25cm}
		\large\\
		\href{https://gitlab.com/NTkot/parallel_systems_exer2}{Gitlab Project}
		
	\end{center}
\end{titlepage}

\section{Introduction}
The purpose of this project is to partition a set of $N$ points, represented with $d$ dimensions each, into $p$ subsets. The metric used to distribute each point is the Euclidean distance from a pre-selected pivot point. Each subset will contain the points whose distances from the pivot point are smaller than the next's subset's points. That means that the first subset will contain points that are closer to the pivot point than the points contained in the second subset. Accordingly, the second subset will contain points that are closer to the pivot point than the points in the third subset, etc.

Each subset of points will be contained in a process. Multiple processes will be executed and their communication will be handled via MPI. The goal is to implement a common program for all processes that, using MPI, will communicate to exchange points. The implemented logic must be based on recursive calls of \textit{distribute\_by\_median()} function. The recursive calls are finalized when each process' points (subset) are partitioned in a way that their distance from the pivot point is smaller compared to the points of the next index process.

\section{Files and build}
The logic is implemented in \textit{C} and the code can be found \href{https://gitlab.com/NTkot/parallel_systems_exer2}{\textbf{here}}. To make the program simpler to implement, several assumptions are taken. First of all, the number of points ($N$) and the number of processes ($p$) \textbf{must be a power of 2}. Secondly, the program accepts certain type of datasets. The file that contains the points \textbf{must declare in the first line the number of points} $N$ and then the \textbf{number of dimensions} $d$. To ease up the user, the MNIST dataset has been preprocessed and files containing MNIST sample points can be found \href{https://drive.google.com/drive/folders/138wVknRKhP0rIpIxYM7dywSOVuZc7rf-?usp=sharing}{\textbf{here}}. Each dataset has a power of 2 points, to comply with the first assumption.

After cloning the files, to build the project, simply run \textit{make} in the project's directory. To run the \textit{main} executable, use the following syntax using \textit{mpirun}:
\begin{lstlisting}[language=bash]
       mpirun -n <number_processes> main <filepath_to_dataset>
\end{lstlisting}
If you want to run more processes than your physical processor cores, you can use \textit{--oversubscribe} flag:
\begin{lstlisting}[language=bash]
mpirun --oversubscribe -n <number_processes> main <filepath_to_dataset>
\end{lstlisting}

\section{Project structure}
\subsection{Program structure}
The program is split into three source files: \textit{main.c}, \textit{mpi\_dist.c} and \textit{utils.c}. The \textit{main.c} file contains the \textit{main} function used, which is responsible for the pre-process of the dataset and initiates the recursive call to \textit{distribute\_by\_median()} function. The \textit{mpi\_dist.c} file contains the implementation of the aforementioned function as well as other function implementations that contribute to the project's goal. Lastly, \textit{utils.c} contains descriptions of miscellaneous functions used for reading dataset files, memory swapping and printing arrays.

\subsection{Code structure}
\subsubsection{Main execution flow}
Main function starts by reading dataset's metadata (number of points and dimensions). Then, memory is allocated for the 2D points matrix each process has (\textit{part\_of\_array}, $N/p$ points). The allocation must result in a contiguous 2D array to allow for a single \textit{MPI\_Send} command to send part of the 2D array. Memory is, also, allocated for the distance array whose i-th element shows the distance between the i-th point and the pivot point. After the allocation is complete, the dataset file is read again and the points included in the file are stored in \textit{part\_of\_array} matrix.

The process with world-ID = 0 selects the first point from it's subset and broadcasts it to the other processes. Then, each process calculates the distance array between its points and the pivot point.

Before calling \textit{distribute\_by\_median()}, the processes are synchronized (using \textit{MPI\_Barrier()}) to account for a common timing counter. When the program returns from \textit{distribute\_by\_median()}, all the points are partitioned among the processes the way it is described in the introduction of this document.

The processes are re-synchronized and the total time spent is counted. Finally, the resulted partition is checked using only the points matrix and the pivot point. Basically, the distances are re-calculated, and the mininum and maximum distances are found in each process. The maximum distance from the pivot point in a process must be smaller or equal to the minimum distance of the next (ID-wise) process. If this is true for all processes, the partition of the points is valid.

\subsubsection{Recursion}
The logic of the partition is implemented inside the recursive function \textit{distribute\_by\_median()}. The function starts by creating the group each process belongs to. The group is comprised of the processes that participate in their corresponding partition group (\textit{processes\_in\_current\_dist} variable). The recursion starts with all the processes belonging to the same group. In the next call, the processes are split into two groups, with each group partitioning their collective data. The next call splits the processes into four groups, the next into eight, etc. The recursion stops when the groups contain only one process each. A communicator is created between the processes in the same group, to allow for easy transfer between processes belonging to the same group. Each process, apart from its world-ID variable, has a local-ID value, signifying its ID inside the group (and not the world, like the world-ID signifies).

In the next code segment, the leader of each group (\textit{local\_id = 0}), gathers all the distances from the processes belonging to the same group. Subsequently, the leader calculates the median from the distances accumulated from the group using \textit{kth\_smallest} method (quickselect). Then, it broadcasts the median distance to all the processes in the same group. Each process partitions its distance array (and its points matrix concurrently) based on the median distance sent by the leader (using \textit{partition\_external\_distance\_pivot} method). Meanwhile, each process counts the number of the distances that are smaller than the group's median distance (\textit{left\_elements} variable).

After each group partitions its data based on the collective median distance, the leader gathers the number of the elements that are smaller and higher from the median distance from each process in the segment and stores them appropriately (\textit{accumulated\_left\_elements} and \textit{accumulated\_right\_elements}). The leader uses this information to create an exchange logic between the processes in the same group. To do this, the processes in each group are split in half. The \textit{smaller} procceses (local-ID wise) are meant to receive the points whose distances are smaller than the group's median distance, while the \textit{higher} processes (local-ID wise) are meant to receive the points whose distances are higher than the group's median distance. This is the goal of each recursion. To split the points of each group between the processes, so, after the exchange takes place, half of the processes contain points that are closer to the pivot point than the points that are found in the processes of the other half.

Finally, the step of the exchange of data takes place. In each iteration, the IDs of the processes that contribute to the current exchange are taken (\textit{lower\_id} and \textit{upper\_id}) and synchronous sends (\textit{MPI\_Ssend()}) and receives (\textit{MPI\_Recv()}) take place between their distance arrays and points matrices. The recursion takes place again if the number of processes in the group will not be 1, where the recursion finalizes.

\section{Results}
The above program was used with the preprocessed MNIST dataset examples, mentioned in the introduction and found \href{https://drive.google.com/drive/folders/138wVknRKhP0rIpIxYM7dywSOVuZc7rf-?usp=sharing}{here}. The system that was tested comprises of a 6-Core (12-Threads) CPU.
\newpage
\begin{figure}[h!]
	\centering
	\vspace{-1cm}
	\includegraphics[width=14.75cm,keepaspectratio]{images/mnist_1024.png}
	\vspace{-0.15cm}
	\caption{Times required to process MNIST dataset containing 1024 sample points.}
\end{figure}

\begin{figure}[h!]
	\centering
	\vspace{-0.35cm}
	\includegraphics[width=14.75cm,keepaspectratio]{images/mnist_16384.png}
	\vspace{-0.15cm}
	\caption{Times required to process MNIST dataset containing 16384 sample points.}
\end{figure}

\begin{figure}[h!]
	\centering
	\vspace{-0.35cm}
	\includegraphics[width=14.75cm,keepaspectratio]{images/mnist_65536.png}
	\vspace{-0.15cm}
	\caption{Times required to process MNIST dataset containing 65536 sample points.}
\end{figure}

As we can see, the time needed to process the dataset increases with the number of processes. This is logical, since the depth of the recursion increases as the number of processes increases.
\end{document}