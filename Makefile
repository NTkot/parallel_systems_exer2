COMPILER=mpicc
FLAGS=-O2

all: main

main: main.o utils.o mpi_dist.o
	${COMPILER} ${FLAGS} main.o utils.o mpi_dist.o -o main -lm

main.o: main.c
	${COMPILER} -c ${FLAGS} main.c

utils.o: utils.c
	${COMPILER} -c ${FLAGS} utils.c

mpi_dist.o: mpi_dist.c
	${COMPILER} -c ${FLAGS} mpi_dist.c

clean:
	rm *.o main test