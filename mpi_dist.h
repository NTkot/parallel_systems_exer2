#ifndef MPI_DIST_H_INCLUDED
#define MPI_DIST_H_INCLUDED

unsigned int pivot_select();
void calculate_pivot_distances(double **part_of_array, double *pivot_point, unsigned int N, unsigned int d, double *distances);
unsigned int partition(double *arr, unsigned int low, unsigned int high);
double kth_smallest(double *a, unsigned int left, long int right, unsigned int k);
unsigned int partition_external_distance_pivot(double *distances, double **points, unsigned int size, double pivot);
void distribute_by_median(double **distances, double **part_of_points, unsigned int N, unsigned int d, unsigned int number_of_processes);
unsigned int create_exchange_arrays(unsigned int *left_elements_arr, 
                                    unsigned int *right_elements_arr,
                                    unsigned int processes_in_current_dist,
                                    unsigned int *exchange_lower_arrays_ids, 
                                    unsigned int *exchange_upper_arrays_ids, 
                                    unsigned int *exchange_number_of_elements);
int check_output(double **part_of_array_distributed, double *pivot_point, unsigned int N, unsigned int d);

#endif