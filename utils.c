#include <stdio.h>
#include <stdlib.h>
#include <mpi/mpi.h>
#include "utils.h"

void read_dataset_metadata(char* filename, unsigned int *number_of_ponts, unsigned int *number_of_dimensions) {
    FILE *f;
    if ((f = fopen(filename, "r")) == NULL) {
        printf("File read failed\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    fscanf(f, "%u %u\n", number_of_ponts, number_of_dimensions);
    fclose(f);
}

int check_inputs(int N, int p) {
    int id;
    MPI_Comm_rank(MPI_COMM_WORLD, &id);

    int k = (p > 1) ? p : 3;
    while(k != 1) {
        if(k % 2 != 0) {
            printf("ID%d: Number of workers is NOT power of 2 or is only one\n", id);
            return 0;
        }
        k /= 2;
    }

    k = (N > 1) ? N : 3;
    while(k != 1) {
        if(k % 2 != 0) {
            printf("ID%d: Number of points is NOT power of 2 or is only one\n", id);
            return 0;
        }
        k /= 2;
    }

    return 1;
}

void read_dataset_part_of_array(char* filename, double** output) {
    FILE *f;
    int N, d, p, id;

    if ((f = fopen(filename, "r")) == NULL) {
        printf("File read failed\n");
        MPI_Abort(MPI_COMM_WORLD, 1);
    }
    fscanf(f, "%d %d\n", &N, &d);
    MPI_Comm_size(MPI_COMM_WORLD, &p);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);

    char c = (char)32;
    int linecount = 0;
    while(c != EOF) {
        if(linecount == (N/p)*id) {
            for(int i = 0; i < (N/p); i++) {
                for(int j = 0; j < d; j++) {
                    if(fscanf(f, "%lf", &output[i][j]) != 1)
                        MPI_Abort(MPI_COMM_WORLD, 3);
                }
            }
            break;
        }
        c = fgetc(f);
        if(c=='\n')
            linecount++;
    }
    fclose(f);

    // if(id == (p-1)) {
    //     for(int i = 0; i < (N/p); i++) {
    //         for(int j = 206; j < 213; j++) {
    //             printf("%.4lf ", output[i][j]);
    //         }
    //         printf("\n");
    //     }
    // }
}

void swap(double *a, double *b) {
    double temp = *a;
    *a = *b;
    *b = temp;
}

void swap_rows(double **arr, unsigned int row1, unsigned int row2) {
    double *temp = arr[row1];
    arr[row1] = arr[row2];
    arr[row2] = temp;
}

void print_array(double *arr, unsigned int size, unsigned int local_id) {
    unsigned int id;
    MPI_Comm_rank(MPI_COMM_WORLD, &id);
    printf("ID%d (id%u): [", id, local_id);
    for(int i = 0; i < size; i++)
        printf("%.4lf ", arr[i]);
    printf("\b]\n");
}

void print_2d_array(double **arr, unsigned int n, unsigned int m, unsigned int local_id) {
    unsigned int id;
    MPI_Comm_rank(MPI_COMM_WORLD, &id);
    printf("ID%d (id%u):\n", id, local_id);
    for(int i = 0; i < n; i++)
        for(int j = 0; i < m; j++)
            printf("%.4lf ", arr[i][j]);
    printf("\b\n");
}