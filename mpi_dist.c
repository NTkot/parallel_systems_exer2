#include <mpi/mpi.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include "mpi_dist.h"
#include "utils.h"

// #define DEBUG

unsigned int pivot_select() {
    return 0;
}

void calculate_pivot_distances(double **part_of_array, double *pivot_point, unsigned int N, unsigned int d, double *distances) {
    unsigned int p, id;
    MPI_Comm_size(MPI_COMM_WORLD, &p);
    MPI_Comm_rank(MPI_COMM_WORLD, &id);

    for(int i = 0; i < (N/p); i++) {
        for(int j = 0; j < d; j++) {
            distances[i] += (part_of_array[i][j] - pivot_point[j]) * (part_of_array[i][j] - pivot_point[j]);
        }
        distances[i] = sqrt(distances[i]);
    }
}

unsigned int partition(double *arr, unsigned int low, unsigned int high) {
    unsigned int i = low;
    double pivot = arr[high];
    for (unsigned int j = low; j < high; j++) {
        if(arr[j] < pivot) {
            swap(&arr[i], &arr[j]);
            i++;
        }
    }

    swap(&arr[i], &arr[high]);
    return i;
}

double kth_smallest(double *a, unsigned int left, long int right, unsigned int k) {
    unsigned int pivotIndex;
    while (left <= right) {
        pivotIndex = partition(a, left, right);
 
        if (pivotIndex == (k - 1))
            return a[pivotIndex];
        else if (pivotIndex < (k - 1))
            left = pivotIndex + 1;
        else
            right = pivotIndex - 1;
    }
    return -1;
}

unsigned int partition_external_distance_pivot(double *distances, double **points, unsigned int size, double pivot) {
    unsigned int low = 0;
    long int high = size - 1;

    while(low < high) {
        while(low < size && distances[low] <= pivot)
            low++;
        while(high > 0 && distances[high] > pivot)
            high--;
        if(low < high) {
            swap(&distances[low], &distances[high]);
            swap_rows(points, low, high);
        }
    }
    return low;   
}

void distribute_by_median(double **distances, double **part_of_points, unsigned int N, unsigned int d, unsigned int processes_in_current_dist) {
    unsigned int p, world_id, local_id;
    unsigned int left_elements, right_elements, *accumulated_left_elements, *accumulated_right_elements;
    unsigned int *exchange_lower_arrays_ids, *exchange_upper_arrays_ids, *exchange_number_of_elements, number_of_exchanges;
    double *accumulated_distances, median_dist;
    MPI_Group world_group, new_group;
    MPI_Comm partition_communicator;
    MPI_Comm_size(MPI_COMM_WORLD, &p);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_id);
    MPI_Comm_group(MPI_COMM_WORLD, &world_group);

    #ifdef DEBUG
    if(world_id == 0)
        printf("\n\nID%u (id%u): CURRENT PROCESSES IN DISTRIBUTION = %u\n", world_id, local_id, processes_in_current_dist);
    #endif

    // Create MPI group/communicator with the purpose of using MPI transfers only between processes that partake in the same partition
    int *ranks = (int *)malloc(processes_in_current_dist * sizeof(int));
    if(ranks == NULL)
        MPI_Abort(MPI_COMM_WORLD, 4);
    for(int i = 0; i < processes_in_current_dist; i++)
        ranks[i] = (world_id / processes_in_current_dist)*processes_in_current_dist + i;
    MPI_Group_incl(world_group, processes_in_current_dist, ranks, &new_group);
    MPI_Comm_create(MPI_COMM_WORLD, new_group, &partition_communicator); 
    MPI_Comm_rank(partition_communicator, &local_id);
    MPI_Barrier(partition_communicator);
    free(ranks);

    // Leader allocates memory for accumulated_distances array to hold distances received from the same group processes
    if(local_id == 0) {
        accumulated_distances = malloc((N/p) * processes_in_current_dist * sizeof(double));
        if(accumulated_distances == NULL)
            MPI_Abort(MPI_COMM_WORLD, 5);
    }
    MPI_Barrier(partition_communicator);
    MPI_Gather(*distances, (N/p), MPI_DOUBLE, accumulated_distances, (N/p), MPI_DOUBLE, 0, partition_communicator);
    // Now leader processes contain all the distances from the processes that partake in their corresponding group partition/distribution

    if(local_id == 0) {
        // print_array(accumulated_distances, (N/p) * processes_in_current_dist, local_id);

        double median_dist1 = kth_smallest(accumulated_distances, 0, (N/p) * processes_in_current_dist - 1, ((N/p) * processes_in_current_dist)/2);
        double median_dist2 = kth_smallest(accumulated_distances, 0, (N/p) * processes_in_current_dist - 1, ((N/p) * processes_in_current_dist)/2 + 1);
        median_dist = (median_dist1 + median_dist2) / 2;

        // printf("ID%d (id%u): Median Distance = %lf\n", world_id, local_id, median_dist);

        // print_array(accumulated_distances, (N/p) * processes_in_current_dist, local_id);
        free(accumulated_distances);
    }
    MPI_Barrier(partition_communicator);
    MPI_Bcast(&median_dist, 1, MPI_DOUBLE, 0, partition_communicator);
    
    // printf("ID%d (id%u): Median Distance = %lf\n", world_id, local_id, median_dist);
    // print_array(*distances, (N/p), local_id);

    left_elements = partition_external_distance_pivot(*distances, part_of_points, (N/p), median_dist);
    right_elements = (N/p) - left_elements;

    // printf("\n");
    // printf("ID%d (id%u): Left elements  = %u\n", world_id , local_id, left_elements);
    // printf("ID%d (id%u): Right elements = %u\n", world_id , local_id, right_elements);
    // print_array(*distances, (N/p), local_id);
    // printf("\n");

    MPI_Barrier(partition_communicator);
    if(local_id == 0) {
        accumulated_left_elements  = malloc(processes_in_current_dist * sizeof(unsigned int));
        accumulated_right_elements = malloc(processes_in_current_dist * sizeof(unsigned int));
        if(accumulated_left_elements == NULL || accumulated_right_elements == NULL)
            MPI_Abort(MPI_COMM_WORLD, 6);
    }
    MPI_Barrier(partition_communicator);
    MPI_Gather(&left_elements,  1, MPI_UNSIGNED, accumulated_left_elements,  1, MPI_UNSIGNED, 0, partition_communicator);
    MPI_Gather(&right_elements, 1, MPI_UNSIGNED, accumulated_right_elements, 1, MPI_UNSIGNED, 0, partition_communicator);

    // if(local_id == 0) {
    //     printf("ID%d (iMPI_Barrieri < processes_in_current_dist; i++)
    //         printf("%u ", accumulated_right_elements[i]);
    //     printf("\b]\n");
    // }

    exchange_lower_arrays_ids   = calloc(processes_in_current_dist, sizeof(unsigned int));
    exchange_upper_arrays_ids   = calloc(processes_in_current_dist, sizeof(unsigned int));
    exchange_number_of_elements = calloc(processes_in_current_dist, sizeof(unsigned int));
    if(exchange_lower_arrays_ids == NULL || exchange_upper_arrays_ids == NULL || exchange_number_of_elements == NULL)
        MPI_Abort(MPI_COMM_WORLD, 7);

    MPI_Barrier(partition_communicator);
    if(local_id == 0) {
        number_of_exchanges = create_exchange_arrays(accumulated_left_elements, 
                                                     accumulated_right_elements, 
                                                     processes_in_current_dist, 
                                                     exchange_lower_arrays_ids, 
                                                     exchange_upper_arrays_ids, 
                                                     exchange_number_of_elements);
        exchange_lower_arrays_ids   = (unsigned int*)realloc(exchange_lower_arrays_ids,   number_of_exchanges*sizeof(unsigned int));
        exchange_upper_arrays_ids   = (unsigned int*)realloc(exchange_upper_arrays_ids,   number_of_exchanges*sizeof(unsigned int));
        exchange_number_of_elements = (unsigned int*)realloc(exchange_number_of_elements, number_of_exchanges*sizeof(unsigned int));

        if((exchange_lower_arrays_ids == NULL || exchange_upper_arrays_ids == NULL || exchange_number_of_elements == NULL) && number_of_exchanges != 0)
            MPI_Abort(MPI_COMM_WORLD, 8);

        #ifdef DEBUG
        printf("ID%d (id%u): Total exchanges = %u\n", world_id, local_id, number_of_exchanges);
        printf("ID%d (id%u): Exchange Lower Arrays IDs = [", world_id, local_id);
        for(int i = 0; i < number_of_exchanges; i++)
            printf("%u ", exchange_lower_arrays_ids[i]);
        printf("\b]\n");
        printf("ID%d (id%u): Exchange Upper Arrays IDs = [", world_id, local_id);
        for(int i = 0; i < number_of_exchanges; i++)
            printf("%u ", exchange_upper_arrays_ids[i]);
        printf("\b]\n");
        printf("ID%d (id%u): Exchange Number of Elements = [", world_id, local_id);
        for(int i = 0; i < number_of_exchanges; i++)
            printf("%u ", exchange_number_of_elements[i]);
        printf("\b]\n\n");
        #endif

        free(accumulated_left_elements);
        free(accumulated_right_elements);
    }

    MPI_Barrier(partition_communicator);
    MPI_Bcast(&number_of_exchanges, 1, MPI_UNSIGNED, 0, partition_communicator);
    MPI_Bcast(exchange_lower_arrays_ids,   number_of_exchanges, MPI_UNSIGNED, 0, partition_communicator);
    MPI_Bcast(exchange_upper_arrays_ids,   number_of_exchanges, MPI_UNSIGNED, 0, partition_communicator);
    MPI_Bcast(exchange_number_of_elements, number_of_exchanges, MPI_UNSIGNED, 0, partition_communicator);

    if(local_id != 0) {
        exchange_lower_arrays_ids   = (unsigned int*)realloc(exchange_lower_arrays_ids,   number_of_exchanges*sizeof(unsigned int));
        exchange_upper_arrays_ids   = (unsigned int*)realloc(exchange_upper_arrays_ids,   number_of_exchanges*sizeof(unsigned int));
        exchange_number_of_elements = (unsigned int*)realloc(exchange_number_of_elements, number_of_exchanges*sizeof(unsigned int));
        if((exchange_lower_arrays_ids == NULL || exchange_upper_arrays_ids == NULL || exchange_number_of_elements == NULL) && number_of_exchanges != 0)
            MPI_Abort(MPI_COMM_WORLD, 9);
    }
    MPI_Barrier(partition_communicator);
    
    // printf("ID%d (id%u): Total exchanges = %u\n", world_id, local_id, number_of_exchanges);
    // printf("ID%d (id%u): Exchange Lower Arrays IDs = [", world_id, local_id);
    // for(int i = 0; i < number_of_exchanges; i++)
    //     printf("%u ", exchange_lower_arrays_ids[i]);
    // printf("\b]\n");
    // printf("ID%d (id%u): Exchange Upper Arrays IDs = [", world_id, local_id);
    // for(int i = 0; i < number_of_exchanges; i++)
    //     printf("%u ", exchange_upper_arrays_ids[i]);
    // printf("\b]\n");
    // printf("ID%d (id%u): Exchange Number of Elements = [", world_id, local_id);
    // for(int i = 0; i < number_of_exchanges; i++)
    //     printf("%u ", exchange_number_of_elements[i]);
    // printf("\b]\n\n");
    
    MPI_Status recv_status;
    for(int i = 0; i < number_of_exchanges; i++) {
        unsigned int lower_id = exchange_lower_arrays_ids[i];
        unsigned int upper_id = exchange_upper_arrays_ids[i];
        unsigned int num_of_elements = exchange_number_of_elements[i];

        /* Exchange data between exchange_lower_arrays_ids[i] and exchange_upper_arrays_ids[i] */
        if(local_id == lower_id || local_id == upper_id) {
            double *distances_buffer = (double *)calloc(num_of_elements, sizeof(double));
            double **points_buffer = (double**)malloc(num_of_elements * sizeof(double*));
            if(distances_buffer == NULL || points_buffer == NULL)
                MPI_Abort(MPI_COMM_WORLD, 10);
            points_buffer[0] = (double*)malloc(num_of_elements * d * sizeof(double));
            for (unsigned int k = 1; k < num_of_elements; k++) {
                points_buffer[k] = points_buffer[k-1] + d;
                if(points_buffer[k] == NULL)
                    MPI_Abort(MPI_COMM_WORLD, 11);
            }

            if(local_id == lower_id) {
                unsigned int index = (unsigned int)(((int)N/p) - ((int)right_elements));
                // printf("ID%u (id%u): lower index = %u\n", world_id, local_id, index);

                MPI_Ssend(&((*distances)[index]), num_of_elements, MPI_DOUBLE, upper_id, i, partition_communicator);
                // MPI_Ssend(&(part_of_points[index][0]), num_of_elements * d, MPI_DOUBLE, upper_id, 2*i, partition_communicator);
                for(int ii = 0; ii < num_of_elements; ii++)
                    MPI_Ssend(part_of_points[index + ii], d, MPI_DOUBLE, upper_id, 2*i, partition_communicator);

                MPI_Recv(distances_buffer, num_of_elements,   MPI_DOUBLE, upper_id, i, partition_communicator, &recv_status);
                // MPI_Recv(&(points_buffer[0][0]), num_of_elements * d, MPI_DOUBLE, upper_id, 2*i, partition_communicator, &recv_status);
                for(int ii = 0; ii < num_of_elements; ii++)
                    MPI_Recv(points_buffer[ii], d, MPI_DOUBLE, upper_id, 2*i, partition_communicator, &recv_status);

                for(unsigned int ii = 0; ii < num_of_elements; ii++) {
                    (*distances)[index + ii] = distances_buffer[ii];
                    for(unsigned int jj = 0; jj < d; jj++) {
                        part_of_points[index + ii][jj] = points_buffer[ii][jj];
                    }
                }
                right_elements -= num_of_elements;
                left_elements  += num_of_elements;

            } else if(local_id == upper_id) {
                unsigned int index = (unsigned int)(((int)N/p) - ((int)right_elements) - ((int)num_of_elements));
                // printf("ID%u (id%u): upper index = %u\n", world_id, local_id, index);
                
                MPI_Recv(distances_buffer, num_of_elements, MPI_DOUBLE, lower_id, i, partition_communicator, &recv_status);
                // MPI_Recv(&(points_buffer[0][0]), num_of_elements * d, MPI_DOUBLE, lower_id, 2*i, partition_communicator, &recv_status);
                for(int ii = 0; ii < num_of_elements; ii++)
                    MPI_Recv(points_buffer[ii], d, MPI_DOUBLE, lower_id, 2*i, partition_communicator, &recv_status);

                MPI_Ssend(&((*distances)[index]), num_of_elements, MPI_DOUBLE, lower_id, i, partition_communicator);
                // MPI_Ssend(&(part_of_points[index][0]), num_of_elements * d, MPI_DOUBLE, lower_id, 2*i, partition_communicator);
                for(int ii = 0; ii < num_of_elements; ii++)
                    MPI_Ssend(part_of_points[index + ii], d, MPI_DOUBLE, lower_id, 2*i, partition_communicator);

                for(unsigned int ii = 0; ii < num_of_elements; ii++) {
                    (*distances)[index + ii] = distances_buffer[ii];
                    for(unsigned int jj = 0; jj < d; jj++) {
                        part_of_points[index + ii][jj] = points_buffer[ii][jj];
                    }
                }
                right_elements += num_of_elements;
                left_elements  -= num_of_elements;
            }

            // free(distances_buffer);
            // for(int k = 0; k < num_of_elements; k++)
            //     free(points_buffer[k]);
            // free(points_buffer);
        }
        MPI_Barrier(partition_communicator);
    }
    free(exchange_lower_arrays_ids);
    free(exchange_upper_arrays_ids);
    free(exchange_number_of_elements);
    
    // printf("ID%d (id%u): Distances = [", world_id, local_id);
    // for(int i = 0; i < (N/p); i++)
    //     printf("%.4lf ", (*distances)[i]);
    // printf("\b]\n");
    
    if(processes_in_current_dist / 2 == 1)
        return;
    else
        distribute_by_median(distances, part_of_points, N, d, processes_in_current_dist / 2);
}

unsigned int create_exchange_arrays(unsigned int *left_elements_arr, 
                                    unsigned int *right_elements_arr, 
                                    unsigned int processes_in_current_dist,
                                    unsigned int *exchange_lower_arrays_ids, 
                                    unsigned int *exchange_upper_arrays_ids, 
                                    unsigned int *exchange_number_of_elements) {
    unsigned int number_of_exchanges = 0;

    unsigned int *left_elements  = malloc(processes_in_current_dist * sizeof(unsigned int));
    unsigned int *right_elements = malloc(processes_in_current_dist * sizeof(unsigned int));
    memcpy(left_elements,  left_elements_arr,  processes_in_current_dist * sizeof(unsigned int));
    memcpy(right_elements, right_elements_arr, processes_in_current_dist * sizeof(unsigned int));

    for(unsigned int lower = 0; lower < processes_in_current_dist/2; lower++) {
        if(right_elements[lower] == 0)
            continue;
        for(unsigned int upper = processes_in_current_dist/2; upper < processes_in_current_dist; upper++) {
            if(left_elements[upper] == 0)
                continue;
                
            long int diff = (long int)right_elements[lower] - (long int)left_elements[upper];
            exchange_lower_arrays_ids[number_of_exchanges] = lower;
            exchange_upper_arrays_ids[number_of_exchanges] = upper;
            if(diff > 0) {
                exchange_number_of_elements[number_of_exchanges] = left_elements[upper];
                right_elements[lower] -= left_elements[upper];
                left_elements[upper] = 0;
                number_of_exchanges++;
                continue;
            } else {
                exchange_number_of_elements[number_of_exchanges] = right_elements[lower];
                left_elements[upper] -= right_elements[lower];
                right_elements[lower] = 0;
                number_of_exchanges++;
                break;
            }
        }
    }
    return number_of_exchanges;
}

int check_output(double **part_of_array_distributed, double *pivot_point, unsigned int N, unsigned int d) {
    double sum = 0, min, max, *min_array, *max_array;
    unsigned int minIndex, maxIndex;
    unsigned int p, world_id;
    int return_value = 1;

    MPI_Comm_size(MPI_COMM_WORLD, &p);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_id);

    for(unsigned int j = 0; j < d; j++) 
        sum += (part_of_array_distributed[0][j] - pivot_point[j]) * (part_of_array_distributed[0][j] - pivot_point[j]);
    sum = sqrt(sum);
    min = sum;
    max = sum;
    minIndex = 1;
    maxIndex = 1;

    for(unsigned int i = 1; i < (N/p); i++) {
        sum = 0;
        for(unsigned int j = 0; j < d; j++)
            sum += (part_of_array_distributed[i][j] - pivot_point[j]) * (part_of_array_distributed[i][j] - pivot_point[j]);
        sum = sqrt(sum);
        if(sum > max) {
            maxIndex = i;
            max = sum;
        } else if(sum < min) {
            minIndex = i;
            min = sum;
        }
    }

    max_array = (double *)calloc(p, sizeof(double));
    min_array = (double *)calloc(p, sizeof(double));
    if(max_array == NULL || min_array == NULL)
        MPI_Abort(MPI_COMM_WORLD, 12);

    MPI_Gather(&max, 1, MPI_DOUBLE, max_array, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Gather(&min, 1, MPI_DOUBLE, min_array, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    if(world_id == 0) {
        printf("Min array =\n");
        print_array(min_array, p, 0);
        printf("Max array =\n");
        print_array(max_array, p, 0);

        for(unsigned int i = 0; i < (p-1); i++) {
            if(max_array[i] > min_array[i+1]) {
                return_value = 0;
                break;
            }
        }
        if(return_value == 0)
            printf("ID%d: DISTRIBUTION NOT VALID!\n", world_id);
        else
            printf("\nID%d: Distribution checked and is valid\n", world_id);
    }
    MPI_Bcast(&return_value, 1, MPI_INT, 0, MPI_COMM_WORLD);
    
    return return_value;
}